# DotNet generation code With Angular Schematics

This sample showing, how do you can use Angular Schematics for DotNet.

Do you have any questions or problems, please contact me: 
- Telegram/Twitter: @yani4218
- email: yani4218@gmail.com

### Getting Started With Schematics

To test locally, create a Schematics project, first install the Schematics CLI:

```bash
npm i -g @angular-devkit/schematics-cli
```

Also сheck the documentation with:

```bash
schematics --help
```

### Build Schematics

`npm run build:schematics` will run building youre schematic.

### Unit Testing

`npm run test` will run the unit tests, using Jasmine as a runner and test framework.

### Publishing

To publish, simply do:

```bash
npm run build:schematics
npm publish
```

### Execute Schematics

- Step 1: Build schematic.
- Step 2: Copied schematic from dist from dist to node_modules youre DotNet project and install npm packages at the schematic folder.
- Step 3: `schematics <path-to-collection.json>:<schematic-name or schematic-aliases> <schematic-options> --dry-run=false` will run execute youre schematic.
Example how run executing schematic, you can look at package.json -> scripts -> dev:сс

### Samples

[Sample-repo](https://gitlab.com/yaniii/schematic-dotnet-sample)

Good luck, my friend!
From scout-ngx team with ❤️