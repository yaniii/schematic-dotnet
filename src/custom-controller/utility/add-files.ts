import {
    Rule,
    applyTemplates,
    apply,
    url,
    move,
    branchAndMerge,
    chain,
    mergeWith
} from '@angular-devkit/schematics';
import { normalize } from '@angular-devkit/core';
import { dasherize, classify } from "@angular-devkit/core/src/utils/strings";

import { schemaOptions } from '../schema';


const stringUtils = { dasherize, classify };

export const addFilesToTree = (_options: schemaOptions): Rule => {

    _options.path = _options.path ? normalize(_options.path) : _options.path;

    const templateSource = apply(url('./files'), [
        applyTemplates({
            ...stringUtils,
            ..._options
        }),
        move(normalize(_options.path as string))
    ]);

    return branchAndMerge(chain([mergeWith(templateSource)]));
}