import { Rule, Tree, SchematicContext } from '@angular-devkit/schematics';

export const addWelcomMessage = (): Rule => {
    return (_tree: Tree, _context: SchematicContext) => {
        _context.logger.log('info', `Hi I'm angular schematic. I will do everything myself, and you could drink ☕️  with 🍩`);
        _context.logger.log('info', `From scout-ngx team with ❤️`);
        return _tree;
    };
}