import { Tree } from '@angular-devkit/schematics';
import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import * as path from 'path';

const collectionPath = path.join(__dirname, '../collection.json');
const runner = new SchematicTestRunner('schematics', collectionPath);

let testTree: UnitTestTree;


describe('react-component', () => {
    beforeEach(() => {
        testTree = runner.runSchematic('custom-controller', {}, Tree.empty());
    });

    it('check add files', () => {
        expect(testTree.files).toEqual(['/Name.cs']);
    });

    it('check file content', () => {
        const fileContentBuffer: Buffer | null = testTree.read('/Name.cs');
        const fileContent: string = fileContentBuffer ? fileContentBuffer.toString() : '';
        expect(fileContent.indexOf('Name') !== -1).toBe(true);
    });
});
